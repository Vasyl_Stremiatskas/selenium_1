import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


    public class Task_1 {

        @Test
        public void wordTest(){

            System.setProperty("webdriver.chome.driver", "../chromedriver.exe");
            WebDriver driver = new ChromeDriver();

            driver.get("https://www.google.com/");
            WebElement element = driver.findElement(By.name("q"));
            element.sendKeys("Apple");
            element.submit();
            System.out.println("Page title is: " + driver.getTitle());

            new WebDriverWait(driver, 10).until(d -> d.getTitle().toLowerCase().startsWith("apple"));
            System.out.println("Page title is: " + driver.getTitle());
            driver.quit();
        }
    }


